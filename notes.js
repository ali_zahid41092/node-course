const fs = require('fs');
const yargs = require('yargs');
const chalk = require("chalk");

const getNotes = function () {
    return 'Your notes...'
}

const addNote = function(title, body){
    const notes = loadNotes();
    const duplicate = notes.filter(function(note) {
        return note.title === title;
    })

    if(duplicate.length === 0) {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes);
    }
    else {
        console.log("Error! Title already exists");
    }

    
}

const removeNotes = function(title) {
    const notes = loadNotes();
    const notestokeep = notes.filter(function(note) {
        return note.title !== title;
    })
    if(notes.length > notestokeep.length){
        console.log(chalk.green.inverse("Note removed"))
        saveNotes(notestokeep);
    } else {
        console.log(chalk.red.inverse("No note found!"))
    }
}

const saveNotes = function(notes) {
    const dataJson = JSON.stringify(notes);
    fs.writeFileSync('notes.json', dataJson);
}
const loadNotes = function() {
    try {
        const databuffer = fs.readFileSync('notes.json');
        const dataJson = databuffer.toString();
        return JSON.parse(dataJson);
    } catch(e) {
        return []; 
    }
    
}

module.exports = {
    getNotes: getNotes,
    addNote: addNote,
    removeNotes: removeNotes
}